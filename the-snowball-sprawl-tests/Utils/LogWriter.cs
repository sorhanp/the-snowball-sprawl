﻿using Moq;
using Xunit;
using System;

using Utils.Output;

namespace UtilsOutputTests
{
    public class LogWriterTests
    {
        LogWriter logWriter;
        Mock<ILog> mockLog = new Mock<ILog>();
        string message = "Test";

        public LogWriterTests()
        {
            logWriter = new LogWriter(mockLog.Object);
        }

        [Fact]
        public void MessageWithoutType()
        {
            logWriter.Message(message);

            mockLog.Verify(x => x.Message(LogType.None, ref message), Times.Exactly(1));
        }

        [Fact]
        public void DebugMessage()
        {
            logWriter.Debug(message);

            mockLog.Verify(x => x.Message(LogType.Debug, ref message), Times.Exactly(1));
        }

        [Fact]
        public void WarningMessage()
        {
            logWriter.Warning(message);

            mockLog.Verify(x => x.Message(LogType.Warning, ref message), Times.Exactly(1));
        }

        [Fact]
        public void ErrorMessage()
        {
            logWriter.Error(message);

            mockLog.Verify(x => x.Message(LogType.Error, ref message), Times.Exactly(1));
        }

        [Fact]
        public void FatalMessage()
        {
            logWriter.Fatal(message);

            mockLog.Verify(x => x.Message(LogType.Fatal, ref message), Times.Exactly(1));
        }
    }
}

/*
 * LogWriter utilizes Log -class's ability to format text. 
 * User of the class only needs to create LogWriter instance and 
 * use it's methods to print text to console(via writeline) / 
 */
