﻿using Xunit;
using Microsoft.Xna.Framework.Input;

namespace UtilsInputTests
{
    public class KeyboardTests
    {
        Utils.Input.Keyboard keyboard;
        public KeyboardTests() 
        {
            keyboard = new Utils.Input.Keyboard();
        }

        [Fact]
        public void DetectAPress()
        {
            KeyboardState kstate = new KeyboardState(Keys.A);

            Assert.True(keyboard.IsPressed(ref kstate, Keys.A));
        }

        [Fact]
        public void FailedToDetectAPress()
        {
            KeyboardState kstate = new KeyboardState(Keys.B);

            Assert.False(keyboard.IsPressed(ref kstate, Keys.A));
        }

        [Fact]
        public void IsAReleased()
        {
            KeyboardState kstate = new KeyboardState(Keys.A);

            Assert.True(keyboard.IsPressed(ref kstate, Keys.A));
            Assert.False(keyboard.Released(ref kstate, Keys.A));
        }

        [Fact]
        public void AIsNotReleased()
        {
            KeyboardState kstate = new KeyboardState(Keys.B);

            Assert.True(keyboard.IsPressed(ref kstate, Keys.B));
            Assert.True(keyboard.Released(ref kstate, Keys.A));
        }
        
        [Fact]
        public void IsAHeld()
        {
            KeyboardState kstate = new KeyboardState(Keys.A);

            Assert.True(keyboard.IsPressed(ref kstate, Keys.A));
            Assert.True(keyboard.IsHeld(ref kstate, Keys.A));
        }
        
        [Fact]
        public void FailedToAHeld() 
        {
            KeyboardState kstate = new KeyboardState(Keys.B);

            Assert.True(keyboard.IsPressed(ref kstate, Keys.B));
            Assert.False(keyboard.IsHeld(ref kstate, Keys.A));
        }
    }
}
