﻿using Xunit;
using System;

using Utils.Output;

namespace UtilsOutputTests
{
    public class LogTests
    {
        // Create log object
        Log log;
        // Variables for tests
        string testString = "test";
        readonly string orgString;

        public LogTests() 
        {
            orgString = testString;
            log = new Log(); 
        }

        [Fact]
        public void NoLogType_AddTimeStampToMessage()
        {
            log.Message(ref testString);
            Assert.Equal(string.Format("{0:s}", DateTime.Now) + ": " + orgString, testString);
        }

        [Fact]
        public void AddDebugLogTypeToMessage()
        {
            log.Message(LogType.Debug, ref testString);
            Assert.Equal(string.Format("{0:s}", DateTime.Now) + "-Debug: " + orgString, testString);
        }

        [Fact]
        public void AddWarningLogTypeToMessage()
        {
            log.Message(LogType.Warning, ref testString);
            Assert.Equal(string.Format("{0:s}", DateTime.Now) + "-Warning: " + orgString, testString);
        }

        [Fact]
        public void AddErrorLogTypeToMessage()
        {
            log.Message(LogType.Error, ref testString);
            Assert.Equal(string.Format("{0:s}", DateTime.Now) + "-Error: " + orgString, testString);
        }

        [Fact]
        public void AddFatalLogTypeToMessage()
        {
            log.Message(LogType.Fatal, ref testString);
            Assert.Equal(string.Format("{0:s}", DateTime.Now) + "-Fatal: " + orgString, testString);
        }
    }
}

/*
 * Idea is to have five different log levels: none, debug, warning, error, fatal
 * Each log level serve different purpose:
 * None: default print text to console
 * Debug: basic debugging information
 * Ẁarning: prints if something is not right, but doesn't effect usage
 * Error: prints if elements are missing, such as failure to load content
 * Fatal: prints in situations that halt the program
 * User may select the logging level. Every log must have a timestamp!
 */