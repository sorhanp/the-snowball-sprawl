﻿using System;
using Microsoft.Xna.Framework.Input;

namespace Utils.Input
{
    public class Keyboard : IInput
    {
        private Keys previouslyPressedKey = 0;

        public Keyboard() 
        {
            
        }


        public bool IsPressed(ref KeyboardState kstate, Keys key)
        {
            if(kstate.IsKeyDown(key))
            {
                previouslyPressedKey = key;
                return true;
            }
            return false;
        }

        public bool IsHeld(ref KeyboardState kstate, Keys key) 
        {
            if (kstate.IsKeyDown(key))
            {
                if (key == previouslyPressedKey)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Released(ref KeyboardState kstate, Keys key)
        {
            if (kstate.IsKeyDown(key))
            {
                if (key == previouslyPressedKey) 
                {
                    return false;
                }
            }
            return true;
        }

        public bool Update(KeyboardState kstate) 
        {
            

            return true;
        }
    }
}
