﻿using System;
namespace Utils.Output
{
    public class LogWriter
    {
        private ILog log;

        public LogWriter(ILog log)
        {
            this.log = log;
        }

        private void Message(LogType type, string message)
        {
            log.Message(type, ref message);
            Console.WriteLine(message);
        }

        public void Message(string message)
        {
            Message(LogType.None, message);
        }

        public void Debug(string message)
        {
            Message(LogType.Debug, message);
        }

        public void Warning(string message)
        {
            Message(LogType.Warning, message);
        }

        public void Error(string message)
        {
            Message(LogType.Error, message);
        }

        public void Fatal(string message)
        {
            Message(LogType.Fatal, message);
        }
    }
}
