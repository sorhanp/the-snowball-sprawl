﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Utils.Output
{
    public class Video
    {
        private GraphicsDeviceManager graphics;

        public Video(Game game)
        {
            graphics = new GraphicsDeviceManager(game)
            {
                // Set 720p as default desired resolution                 
                // If desired is not achieved fall back to DefaultBackBuffer
                PreferredBackBufferWidth = 1280,
                PreferredBackBufferHeight = 720,

                // Set windowed with Vsync
                IsFullScreen = false,
                SynchronizeWithVerticalRetrace = true,

                // Defines how window switches from windowed to fullscreen state.
                // True = more performance in full screen. False = quicker switch
                HardwareModeSwitch = true,

                // Set GraphicsProfile to reach in order to support large number of platforms
                GraphicsProfile = GraphicsProfile.Reach,
                // Defines multisampled (Anti Aliasing) back buffer
                PreferMultiSampling = false,

                // Defines back buffer color format
                PreferredBackBufferFormat = SurfaceFormat.Color,
                // Defines depth-stencil buffer format
                PreferredDepthStencilFormat = DepthFormat.Depth24,
            };
            Console.WriteLine("Debug: " + graphics.SupportedOrientations);
        }

        // Property to access to GraphicsDevice
        public GraphicsDevice GraphicsDevice
        {
            get => graphics.GraphicsDevice;
        }

        // TODO: Use setting manager to load use stored graphics settings

        public bool SetFullScreen(int width, int height)
        {
            graphics.PreferredBackBufferWidth = width;
            graphics.PreferredBackBufferHeight = height;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            return true;
        }
    }
}
