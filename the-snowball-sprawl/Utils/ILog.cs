﻿using System;
namespace Utils.Output
{
    public enum LogType { None, Debug, Warning, Error, Fatal };

    public interface ILog
    {
        void Message(LogType type, ref string message);
    }
}
