﻿using System;
namespace Utils.Output
{

    public class Log: ILog
    {
        public void Message(ref string message)
        {
           this.Message(LogType.None, ref message);
        }

        public void Message(LogType type, ref string message)
        {
            string logType = null;

            switch(type)
            {
                case LogType.Debug:
                {
                    logType = "-Debug: ";
                    break;
                }
                case LogType.Warning:
                {
                    logType = "-Warning: ";
                    break;
                }
                case LogType.Error:
                {
                    logType = "-Error: ";
                    break;
                }
                case LogType.Fatal:
                {
                    logType = "-Fatal: ";
                    break;
                }

                default:
                {
                    logType = ": ";
                    break;
                }
            }
            // Log format: <Current DateTime in {0:s} -format>-<logtype>: <message>
            message = string.Format("{0:s}", DateTime.Now) + logType + message;
        }
    }
}
